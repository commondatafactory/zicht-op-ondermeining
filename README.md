Dataset from zicht op ondermeining.nl

visible heare using pg_tileserver:

https://tileserver.commondatafactory.nl/zoo.wijk_autobranche.html


Importing Raw Data from geojson

steps 1 geojson

```
  ogr2ogr -f PostgreSQL 'PG:host=127.0.0.1 dbname=cdf user=cdf port=5433 password=insecure' gemeente.geojson -nln zoo.gemeente
```

step 2 the csv data.


```
pgfutter --ignore-errors=0 --port 5433 --dbname cdf --username cdf --pass insecure csv handel_autos.csv --skip-header --delimiter ";"
```


step 3 run the provided sql in the zoo.sql.


We create for each geometric object we convert all different variables into one single row using a crostable.
!! take great care to getting the right values into the right columns. !!

There are 24 columns possible:

```
branche_omzet_inw_2015_2018_abs
branche_omzet_inw_2015_2018_pct
branche_omzet_inw_2015_concentratie
branche_omzet_inw_2016_concentratie
branche_omzet_inw_2017_concentratie
branche_omzet_inw_2018_concentratie
branche_omzet_promil_2015_2018_abs
branche_omzet_promil_2015_2018_pct
branche_omzet_promil_2015_concentratie
branche_omzet_promil_2016_concentratie
branche_omzet_promil_2017_concentratie
branche_omzet_promil_2018_concentratie
branche_vst_1000vst_2015_2018_abs
branche_vst_1000vst_2015_2018_pct
branche_vst_1000vst_2015_concentratie
branche_vst_1000vst_2016_concentratie
branche_vst_1000vst_2017_concentratie
branche_vst_1000vst_2018_concentratie
branche_vst_10k_inw_2015_2018_abs
branche_vst_10k_inw_2015_2018_pct
branche_vst_10k_inw_2015_concentratie
branche_vst_10k_inw_2016_concentratie
branche_vst_10k_inw_2017_concentratie
branche_vst_10k_inw_2018_concentratie
```
