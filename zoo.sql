create extension postgis;
CREATE EXTENSION IF NOT EXISTS tablefunc;


/* select values for one statcode */
select * from zoo.handel_autos ha order by concat(maat, periode, eenheid) desc;


/* select columns for crosstab */
select distinct(concat(maat,'_', periode, '_', eenheid,' ', 'text,')) c from zoo.handel_autos order by c  ;

drop table if exists zoo.crosstabdata;
/* create crosstab convert rows to columns */
select * into zoo.crosstabdata from crosstab (
 	E'select statcode, concat(maat,periode,eenheid), 
     case 
		when waarde = \'NA\' then 0.0
		else replace(waarde, \',\', \'.\')::float
	 end as values 
     from zoo.handel_autos ha order by 1,2' )
as (
	statcode text, 
	branche_omzet_inw_2015_2018_abs float,
	branche_omzet_inw_2015_2018_pct float,
	branche_omzet_inw_2015_concentratie float,
	branche_omzet_inw_2016_concentratie float,
	branche_omzet_inw_2017_concentratie float,
	branche_omzet_inw_2018_concentratie float,
	branche_omzet_promil_2015_2018_abs float,
	branche_omzet_promil_2015_2018_pct float,
	branche_omzet_promil_2015_concentratie float,
	branche_omzet_promil_2016_concentratie float,
	branche_omzet_promil_2017_concentratie float,
	branche_omzet_promil_2018_concentratie float,
	branche_vst_1000vst_2015_2018_abs float,
	branche_vst_1000vst_2015_2018_pct float,
	branche_vst_1000vst_2015_concentratie float,
	branche_vst_1000vst_2016_concentratie float,
	branche_vst_1000vst_2017_concentratie float,
	branche_vst_1000vst_2018_concentratie float,
	branche_vst_10k_inw_2015_2018_abs float,
	branche_vst_10k_inw_2015_2018_pct float,
	branche_vst_10k_inw_2015_concentratie float,
	branche_vst_10k_inw_2016_concentratie float,
	branche_vst_10k_inw_2017_concentratie float,
	branche_vst_10k_inw_2018_concentratie float,
	should_be_empty float
)
order by statcode;

select * from zoo.crosstabdata c;

create table as (branche_omzet_inw text, branche_vst_1000vst text, branch_omzet_promil, text)


select statcode, concat(maat, '_', periode) as category, periode, waarde as values from zoo.handel_autos ha order by 1,2;


select count(*) from zoo.crosstabdata;
select count(*) from zoo.wijk;
select count(*) from zoo.gemeente;

select st_setsrid(st_transform(st_setsrid(wkb_geometry, '28992'), '3875'), '3875') from zoo.gemeente;

select *, st_transform(st_setsrid(wkb_geometry, 28992), 3875) from zoo.gemeente g;

drop table if exists zoo.gemeente_autobranche;
select c.*, st_multi(st_transform(st_setsrid(wkb_geometry, 28992), 3875))  as geom
into zoo.gemeente_autobranche
from zoo.crosstabdata c 
left outer join zoo.gemeente g on g.statcode = c.statcode
where g.statcode is not null

alter table zoo.gemeente_autobranche  alter column geom type geometry(MULTIPOLYGON, 3875);
create index on zoo.gemeente_autobranche using gist (geom);
create index on zoo.gemeente_autobranche (statcode);


drop table if exists zoo.wijk_autobranche;
select c.*, st_multi(st_transform(st_setsrid(wkb_geometry, 28992), 3875)) as geom
into zoo.wijk_autobranche
from zoo.crosstabdata c 
left outer join zoo.wijk g on g.statcode = c.statcode
where g.statcode is not null

alter table zoo.wijk_autobranche  alter column geom type geometry(MULTIPOLYGON, 3875);
create index on zoo.wijk_autobranche using gist (geom);
create index on zoo.wijk_autobranche (statcode);


